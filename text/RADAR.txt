{BLACK}→L₆
prgmWALLPAPE
.5→I:8→M

­1.5→S
0→X
­3.5→T
­5→O
Circle(0,0,M+.6,WHITE)
While 1
If X>0
Circle(0,0,X,GREEN)
If S>0
Circle(0,0,S,BLACK)
If T>0
Circle(0,0,T,RED)
If O>0
Circle(0,0,O,DARKGRAY)
I+S→S:I+X→X:I+T→T:I+O→O
If X>M:1→X:If T>M:1→T
If S>M:1→S:If O>M:1→O
End